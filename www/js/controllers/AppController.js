/**
 * Handles application main behaviour.
 *
 * Initializes out starting list with the first one it finds
 */
ctrlModule.controller('AppController', function ($rootScope, $scope, $http, $timeout, LocalLists, ListService) {

  $scope.init = function () {
    var first = Lists.first();
    console.log("Starting with: ", first);
    ListService.selectList(first, $rootScope)
  };

});

