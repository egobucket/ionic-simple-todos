var app = angular.module('app', ['ionic', 'controllers']);
var ctrlModule = angular.module('controllers', ['factories']);
var factModule = angular.module('factories', []);
var direcModule = angular.module('directives', []);

app.run(function ($ionicPlatform) {
  $ionicPlatform.ready(function () {

    // Set to true to enable server sync
    TaskService.enabled = false;

    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
    }
    if (window.StatusBar) {
      StatusBar.styleDefault();
    }
  });
});
