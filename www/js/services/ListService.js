//Shared service to handle controller communication
factModule.factory('ListService', function () {

  /**
   * Saves this service current list to the object
   * and broadcast the onSelectedList event
   * @param list selected
   * @param $scope emitter controller from where the selection was done
   */
  var selectList = function (list, $scope) {
    console.log("Broadcasting:", list);
    this.currentList = list;
    $scope.$emit(this.events.ON_SELECTED_LIST, list)
  };

  /**
   * Deletes current list
   * Selects a new one
   */
  var deleteList = function (list, $scope) {
    this.currentList = null;
    $scope.$emit(this.events.ON_LIST_DELETED, list);
  };

  /**
   * Persists changes of currentList
   * @param $scope emitter controller
   */
  var saveList = function ($scope) {
    $scope.$emit(this.events.ON_LIST_CHANGED, this.currentList);
  };

  return {
    saveList: saveList,
    selectList: selectList,
    deleteList: deleteList,
    events: {ON_SELECTED_LIST: 'onSelectedList', ON_LIST_CHANGED: 'onListChanged', ON_LIST_DELETED: 'onListDeleted'},
    currentList: null
  };
});
