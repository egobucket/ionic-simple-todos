# Ionic Simple Todos

This is a simple *Todos* app to test out [Ionic](http://ionicframework.com)
 
It is inspired by [Meteor Simple Todos](http://todos.meteor.com)
 
## Why 

Challenged to do it.
 
## How to run

- clone repo
- cd into project root
- ``` npm install ```
- ``` bower install ```
- ``` ionic serve ```

## Configure

- (*not required*): I created a little server side api for a naive *server sync*.
To enable it, go into ```/www/js/app.js``` and set ```TaskService.enabled = true;```

## Project 

The ionic.project is configured for development purposes. 

- Angular .js files are automatically built into a single ```app.min.js``` file for ease of access on the index.html.